package com.example.asynchronous;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DetailsAsynchronousController {
	@Autowired
	public DetailsAsynchronousService services;
	
	@GetMapping(value = "/Detailsasynchronous")
	public ResponseEntity<List<DetailsAsynchronous>> getAllDetailsAsynchronous() {
		List<DetailsAsynchronous> list = services.getAllDetailsAsynchronous();
		return ResponseEntity.status(201).body(list);
	}

	@PostMapping(value = "/Detailsasynchronous")
	public ResponseEntity<DetailsAsynchronous> saveDetailsAsynchronous(@RequestBody DetailsAsynchronous storeDetails) {
		DetailsAsynchronous store = services.saveDetailsAsynchronous(storeDetails);
		return ResponseEntity.status(200).body(store);
	}

	@PutMapping(value = "/Detailsasynchronous/{itemname}")
	public ResponseEntity<DetailsAsynchronous> updateDetailsAsynchronous(@RequestBody DetailsAsynchronous storeDetails,
			@PathVariable("itemname") String itemname) {
		storeDetails.setItemname(itemname);

		return 
				ResponseEntity.status(200).body(services.updateDetailsAsynchronous(storeDetails));
	}


}