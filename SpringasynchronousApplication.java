package com.example.asynchronous;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringasynchronousApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringasynchronousApplication.class, args);
	}

}
